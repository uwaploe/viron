// Package viron provides functions to control a Quantel Viron laser
package viron

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"sync"
	"time"
)

type TriggerSource int

const (
	InternalTrigger TriggerSource = 0
	ExternalTrigger               = 1
)

// Status bitfield assignments
//
// State Byte 1
const (
	NotReady uint8 = (1 << iota)
	QswitchEnabled
	BurstMode
	DivideByMode
	QswitchExternal
	DiodeTriggerExternal
	StandbyMode
	FireMode
)

// State Byte 2
const (
	Oven1Running uint8 = (1 << iota)
	Oven2Running
	LanSession
	DiodeTecRunning
	BleSession
	TriggerTermEnabled
	RemoteQswitchOff
	UvEnabled
)

// Fault Byte 1
const (
	LaserInterlock uint8 = (1 << iota)
	SystemTecInterlock
	DiodeTempControl
	DiodeTempRange
	DiodeCurrent
	Charge
	LaserTempRange
	RemoteInterlock
)

// Fault Byte 2
const (
	WatchdogTimeout uint8 = (1 << iota)
	RAMTest
	Runtime
	CANBus
	FactoryCfgChecksum
	OpCfgChecksum
)

// Warning Byte 1
const (
	DiodeTecTemp uint8 = (1 << iota)
	_
	DiodeCurrentLimit
	CANBusOverrun
	CANBusIllegalData
	QswitchInhibited
	LaserTemperature
	ExternalLampPRF
)

// Warning Byte 2
const (
	Oven1OpenSensor uint8 = (1 << iota)
	Oven1OverTemp
	Oven1Timeout
	Oven1OutOfTolerance
	Oven2OpenSensor
	Oven2OverTemp
	Oven2Timeout
	Oven2OutOfTolerance
)

// Limits struct contains the pulse-repetition frequency (PRF) and
// current limits.
type Limits struct {
	// Minimum PRF in Hz
	Minprf int `json:"minprf"`
	// Maximum PRF in Hz
	Maxprf int `json:"maxprf"`
	// Maximum laser diode current in amps
	Maxcurr float64 `json:"maxcurr"`
}

// Status struct contains the parsed output of the STATUS command
type Status struct {
	State   [2]uint8 `json:"state"`
	Fault   [2]uint8 `json:"fault"`
	Warning [2]uint8 `json:"warning"`
}

var NoFault = [2]uint8{0, 0}
var NoWarning = [2]uint8{0, 0}

// Temp struct contains the laser internal temperature values
type Temp struct {
	Laser float32 `json:"laser_temp"`
	Diode float32 `json:"diode_temp"`
}

func (t Temp) String() string {
	return fmt.Sprintf("laser=%.2f, diode=%.2f", t.Laser, t.Diode)
}

type Info struct {
	Status
	Temp
}

// Command errors
var (
	NoResponse    = errors.New("No response from device")
	BadCommand    = errors.New("Invalid command")
	BadValue      = errors.New("Invalid argument(s)")
	OutOfRange    = errors.New("Argument(s) out of range")
	RequiresLogin = errors.New("Login required")
	WrongMode     = errors.New("Not during FIRE mode")
	BadPassword   = errors.New("Invalid login password")
	TimeExpired   = errors.New("Timeout expired")
)

// Device response
type Response struct {
	// Command output (i.e. multiline response)
	Output []string `json:"output"`
	// Return value
	Rval string `json:"return"`
}

// Command line to send to the device
type CommandLine string

// NewCommand constructs a new command line
func NewCommandLine(cmd string, args ...string) CommandLine {
	if len(args) == 0 {
		return CommandLine("$" + strings.ToUpper(cmd))
	}
	return CommandLine(fmt.Sprintf("$%s %s",
		strings.ToUpper(cmd),
		strings.Join(args, " ")))
}

func (c CommandLine) Command() string {
	f := strings.SplitN(string(c), " ", 2)
	return f[0]
}

// Trace contains a command sent to the Device along with the
// returned response.
type Trace struct {
	In  string   `json:"in"`
	Out Response `json:"out"`
}

// Device represents the laser controller
type Device struct {
	port    io.ReadWriter
	respBuf bytes.Buffer
	tnext   time.Time
	mu      *sync.Mutex
}

func NewDevice(port io.ReadWriter) *Device {
	return &Device{
		port: port,
		mu:   &sync.Mutex{}}
}

func (d *Device) readUntil(marker []byte) (string, error) {
	d.respBuf.Reset()
	tail := -len(marker)
	b := make([]byte, 1)
	for {
		_, err := d.port.Read(b)
		if err != nil {
			return d.respBuf.String(), err
		}

		d.respBuf.Write(b)
		tail++
		if tail < 0 {
			continue
		}

		if bytes.Equal(d.respBuf.Bytes()[tail:], marker) {
			return strings.TrimSuffix(d.respBuf.String(), string(marker)), nil
		}
	}

}

// Exec sends a command to the device and returns the response along with
// any error that occurs.
func (d *Device) Exec(c CommandLine) (Response, error) {
	var resp Response

	d.mu.Lock()
	defer d.mu.Unlock()

	// Sleep until the next command time
	time.Sleep(d.tnext.Sub(time.Now()))

	_, err := fmt.Fprint(d.port, c, "\r")
	if err != nil {
		return resp, err
	}
	// Earliest time that another command may be sent. See Section 3.0
	// of the Viron Laser Manual.
	d.tnext = time.Now().Add(time.Millisecond * 50)

	out, err := d.readUntil([]byte(c.Command()))
	if err != nil {
		return resp, fmt.Errorf("%q: %w", c, NoResponse)
	}
	if out != "" {
		resp.Output = strings.Split(strings.TrimRight(out, "\x00"), "\x00")
	}

	text, err := d.readUntil([]byte("\x00"))
	if err != nil {
		return resp, fmt.Errorf("%q: %w", c, NoResponse)
	}

	f := strings.SplitN(strings.TrimRight(text, "\r\x00"), " ", 2)
	if len(f) > 1 {
		switch strings.ToLower(f[1]) {
		case "bad command":
			return resp, fmt.Errorf("%q: %w", c, BadCommand)
		case "bad value":
			return resp, fmt.Errorf("%q: %w", c, BadValue)
		case "out of range":
			return resp, fmt.Errorf("%q: %w", c, OutOfRange)
		case "requires login":
			return resp, fmt.Errorf("%q: %w", c, RequiresLogin)
		case "not during fire":
			return resp, fmt.Errorf("%q: %w", c, WrongMode)
		case "denied":
			return resp, fmt.Errorf("%q: %w", c, BadPassword)
		}
		resp.Rval = f[1]
	}
	return resp, nil
}

// StartSession initiates a new login session
func (d *Device) StartSession(password string) error {
	_, err := d.Exec(NewCommandLine("LOGIN", password))
	return err
}

// Status returns the current controller status bytes
func (d *Device) Status() (Status, error) {
	var s Status
	reply, err := d.Exec(NewCommandLine("STATUS", "?"))
	if err != nil {
		return s, err
	}
	var regs [3]uint16
	_, err = fmt.Sscanf(reply.Rval, "%x %x %x",
		&regs[0], &regs[1], &regs[2])
	if err != nil {
		return s, fmt.Errorf("Unexpected reply; %q: %w",
			reply.Rval, err)
	}

	s.State[0] = uint8(regs[0] >> 8)
	s.State[1] = uint8(regs[0])
	s.Fault[0] = uint8(regs[1] >> 8)
	s.Fault[1] = uint8(regs[1])
	s.Warning[0] = uint8(regs[2] >> 8)
	s.Warning[1] = uint8(regs[2])

	return s, err
}

// Upload sends the contents of a command file to the device and returns
// a list of the sent commands and responses along with any error that
// occurs.
func (d *Device) Upload(cmdfile io.Reader) ([]Trace, error) {
	log := make([]Trace, 0)
	scanner := bufio.NewScanner(cmdfile)
	line := int(0)
	for scanner.Scan() {
		line++
		if scanner.Bytes()[0] == '#' {
			// Skip comment lines
			continue
		}
		fields := strings.Split(strings.TrimRight(scanner.Text(), " \t\r\n"), " ")
		cmd := NewCommandLine(fields[0], fields[1:]...)
		reply, err := d.Exec(cmd)
		log = append(log, Trace{In: string(cmd), Out: reply})
		if err != nil {
			return log, fmt.Errorf("Error on line %d: %w", line, err)
		}
	}

	return log, scanner.Err()
}

// Limits returns the Device PRF and current limits.
func (d *Device) Limits() (Limits, error) {
	var l Limits

	reply, err := d.Exec(NewCommandLine("MINPRF", "?"))
	if err != nil {
		return l, err
	}
	l.Minprf, err = strconv.Atoi(reply.Rval)
	if err != nil {
		return l, err
	}

	reply, err = d.Exec(NewCommandLine("MAXPRF", "?"))
	if err != nil {
		return l, err
	}
	l.Maxprf, err = strconv.Atoi(reply.Rval)
	if err != nil {
		return l, err
	}

	reply, err = d.Exec(NewCommandLine("MAXCURR", "?"))
	if err != nil {
		return l, err
	}
	l.Maxcurr, err = strconv.ParseFloat(reply.Rval, 64)
	if err != nil {
		return l, err
	}

	return l, nil
}

// Temperature returns the internal temperatures
func (d *Device) Temperature() (Temp, error) {
	var temp Temp

	reply, err := d.Exec(NewCommandLine("LTEMF", "?"))
	if err != nil {
		return temp, err
	}

	if reply.Rval != "OFF" {
		x, err := strconv.ParseFloat(reply.Rval, 32)
		if err != nil {
			return temp, err
		}
		temp.Laser = float32(x)
	}

	reply, err = d.Exec(NewCommandLine("DTEMF", "?"))
	if err != nil {
		return temp, err
	}

	if reply.Rval != "OFF" {
		x, err := strconv.ParseFloat(reply.Rval, 32)
		if err != nil {
			return temp, err
		}
		temp.Diode = float32(x)
	}

	return temp, nil
}

// StatusChecker is called everytime the laser Status is polled in the
// WaitForStatus method. It returns true if the wait should end.
type StatusChecker func(Status) bool

// WaitForStatus polls the laser Status until the StatusChecker returns true
// or the Context is cancelled. If ch is non-nil, the laser status and
// temperature will be sent to it each time the device is polled.
func (d *Device) WaitForStatus(ctx context.Context, fn StatusChecker, ch chan<- Info) error {
loop:
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			stat, err := d.Status()
			if err != nil {
				return err
			}
			if ch != nil {
				temp, _ := d.Temperature()
				select {
				case ch <- Info{Status: stat, Temp: temp}:
				default:
				}
			}
			if fn(stat) {
				break loop
			}
		}
	}

	return nil
}

// WaitForStandby runs WaitForStatus with a StatusChecker that returns
// true when the laser enters STANDBY mode.
func (d *Device) WaitForStandby(ctx context.Context, ch chan<- Info) error {
	return d.WaitForStatus(ctx, func(stat Status) bool {
		return (stat.State[0] & StandbyMode) == StandbyMode
	}, ch)
}

// WaitForReady runs WaitForStatus with a StatusChecker that returns
// true when the laser is ready to enter FIRE mode.
func (d *Device) WaitForReady(ctx context.Context, ch chan<- Info) error {
	return d.WaitForStatus(ctx, func(stat Status) bool {
		return (stat.State[0]&StandbyMode) == StandbyMode &&
			stat.Fault == NoFault && stat.Warning == NoWarning
	}, ch)
}

// WaitForNoFault runs WaitForStatus with a StatusChecker that returns
// true when the laser is showing no faults
func (d *Device) WaitForNoFault(ctx context.Context, ch chan<- Info) error {
	return d.WaitForStatus(ctx, func(stat Status) bool {
		return (stat.State[0]&StandbyMode) == StandbyMode &&
			stat.Fault == NoFault
	}, ch)
}
