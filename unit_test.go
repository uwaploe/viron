package viron

import (
	"errors"
	"reflect"
	"strings"
	"testing"
	"time"
)

type port struct {
	w    *strings.Builder
	r    *strings.Reader
	trig chan struct{}
}

func newPort(trigger chan struct{}) *port {
	p := port{
		w:    &strings.Builder{},
		trig: trigger,
	}
	return &p
}

func (p *port) setReader(s string) {
	p.r = strings.NewReader(s)
}

func (p *port) Read(b []byte) (int, error) {
	<-p.trig
	n, err := p.r.Read(b)
	if b[0] == '\n' {
		time.Sleep(time.Millisecond * 50)
	}
	return n, err
}

func (p *port) Write(b []byte) (int, error) {
	return p.w.Write(b)
}

func TestCommand(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	table := []struct {
		in        CommandLine
		out, resp string
		display   []string
	}{
		{
			in:   NewCommandLine("FIRE"),
			out:  "$FIRE\r\x00",
			resp: "",
		},
		{
			in:   NewCommandLine("QSON", "?"),
			out:  "$QSON 1\r\x00",
			resp: "1",
		},
		{
			in:      NewCommandLine("HELLO"),
			out:     "Hello\r\x00World\r\x00$HELLO OK\r\x00",
			resp:    "OK",
			display: []string{"Hello\r", "World\r"},
		},
	}

	d := NewDevice(p)
	var (
		resp Response
		err  error
	)

	for _, e := range table {
		p.setReader(e.out)
		resp, err = d.Exec(e.in)
		if err != nil {
			t.Error(err)
		}
		if resp.Rval != e.resp {
			t.Errorf("Bad response; expected %q, got %q", e.resp, resp)
		}

		if e.display != nil {
			if !reflect.DeepEqual(e.display, resp.Output) {
				t.Errorf("Bad output; expected %q, got %q", e.display, resp.Output)
			}
		} else {
			if len(resp.Output) != 0 {
				t.Errorf("Unexpected output: %q (%d)", resp.Output, len(resp.Output))
			}
		}
	}
}

func TestErrors(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	table := []struct {
		in   CommandLine
		out  string
		resp error
	}{
		{
			in:   NewCommandLine("FOO"),
			out:  "$FOO Bad Command\r\x00",
			resp: BadCommand,
		},
		{
			in:   NewCommandLine("QSON", "x"),
			out:  "$QSON Bad Value\r\x00",
			resp: BadValue,
		},
		{
			in:   NewCommandLine("LOGIN", "12345"),
			out:  "$LOGIN DENIED\r\x00",
			resp: BadPassword,
		},
	}

	d := NewDevice(p)
	var (
		err error
	)

	for _, e := range table {
		p.setReader(e.out)
		_, err = d.Exec(e.in)
		if !errors.Is(err, e.resp) {
			t.Errorf("Bad response; expected %q, got %q", e.resp, err)
		}
	}
}

func TestStatus(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	out := "$STATUS 0102 0304 0506\r\x00"
	expect := Status{
		State:   [2]uint8{0x01, 0x02},
		Fault:   [2]uint8{0x03, 0x04},
		Warning: [2]uint8{0x05, 0x06},
	}

	d := NewDevice(p)
	p.setReader(out)
	s, err := d.Status()
	if err != nil {
		t.Fatal(err)
	}
	if s != expect {
		t.Errorf("Bad response; expected %#v, got %#v", expect, s)
	}
}

func TestTemperature(t *testing.T) {
	table := []struct {
		out  string
		vals Temp
	}{
		{
			out:  "$LTEMF 42.5\r\x00$DTEMF OFF\r\x00",
			vals: Temp{Laser: 42.5},
		},
		{
			out:  "$LTEMF OFF\r\x00$DTEMF OFF\r\x00",
			vals: Temp{},
		},
		{
			out:  "$LTEMF OFF\r\x00$DTEMF 42.5\r\x00",
			vals: Temp{Diode: 42.5},
		},
	}

	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	d := NewDevice(p)
	for _, e := range table {
		p.setReader(e.out)
		vals, err := d.Temperature()
		if err != nil {
			t.Fatal(err)
		}
		if vals != e.vals {
			t.Errorf("Bad value; expected %#v, got %#v", e.vals, vals)
		}
	}
}
